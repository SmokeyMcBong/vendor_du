# Additional packages
PRODUCT_PACKAGES += \
	Development \
	LatinIME \
	VoiceDialer \
	Basic

# Additional apps
PRODUCT_PACKAGES += \
	DashClock \
	DSPManager \
	DU_about \
        DU_changelog \
	libcyanogen-dsp \
	audio_effects.conf \
	MonthCalendarWidget \
        OmniSwitch \
        Terminal

PRODUCT_PACKAGES += \
	CellBroadcastReceiver

# Additional tools
PRODUCT_PACKAGES += \
	openvpn \
	e2fsck \
	mke2fs \
	tune2fs \
	bash \
	vim \
	nano \
	htop \
	powertop \
	lsof \
	mount.exfat \
	fsck.exfat \
	mkfs.exfat

